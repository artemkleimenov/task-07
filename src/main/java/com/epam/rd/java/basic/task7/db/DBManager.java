package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;
	private static Connection connection;
	private static String connectionUrl;
	private static final Logger LOGGER = Logger.getLogger(DBManager.class.getName());

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
			connectionUrl = getConnectionUrl();
		}

		return instance;
	}

	public static Connection getConnection() {
		try {
			connection = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}

		return connection;
	}

	private static String getConnectionUrl() {
		String connectionUrl = "";

		try (FileReader fileReader = new FileReader("app.properties")) {
			Properties properties = new Properties();
			properties.load(fileReader);
			connectionUrl = properties.getProperty("connection.url");
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}

		return connectionUrl;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String query = "SELECT login FROM users ORDER BY id";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					String login = rs.getString(1);
					User user = getUser(login);
					users.add(user);
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't find all users", e);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		String query = "INSERT INTO users VALUES (DEFAULT, ?)";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, user.getLogin());

			if (ps.executeUpdate() != 1) {
				return false;
			}

			try (ResultSet rs = ps.getGeneratedKeys()) {
				if (rs.next()) {
					user.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't insert user", e);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String query = "DELETE FROM users WHERE id = ?";
		int count;

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			conn.setAutoCommit(false);

			for (User user : users) {
				ps.setInt(1, user.getId());
				ps.addBatch();
			}

			count = ps.executeBatch().length;

			if (count == users.length) {
				conn.commit();

				return true;
			} else {
				conn.rollback();

				return false;
			}
		} catch (SQLException e) {
			throw new DBException("Can't delete users", e);
		}
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		String query = "SELECT id, login FROM users WHERE login = ?";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, login);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					user.setId(rs.getInt(1));
					user.setLogin(rs.getString(2));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't get user", e);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		String sql = "SELECT id, name FROM teams WHERE name = ?";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(sql)) {
			ps.setString(1, name);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					team.setId(rs.getInt(1));
					team.setName(rs.getString(2));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't get team", e);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "SELECT name FROM teams ORDER BY id";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					String name = rs.getString(1);
					Team team = getTeam(name);
					teams.add(team);
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't find all teams", e);
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams VALUES(DEFAULT, ?)";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, team.getName());

			if (ps.executeUpdate() != 1) {
				return false;
			}

			try (ResultSet rs = ps.getGeneratedKeys()) {
				if (rs.next()) {
					team.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't insert team", e);
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String query = "INSERT INTO users_teams VALUES (?, ?)";
		int count;

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			setAutoCommit(false);

			for (Team team : teams) {
				ps.setInt(1, user.getId());
				ps.setInt(2, team.getId());
				ps.addBatch();
			}

			count = ps.executeBatch().length;

			if (count == teams.length) {
				conn.commit();
			}

			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				LOGGER.log(Level.SEVERE, ex.getMessage());
			}

			throw new DBException("Can't set teams for user", e);
		} finally {
			setAutoCommit(true);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "SELECT team_id FROM users_teams where user_id = ? ORDER BY team_id";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setInt(1, user.getId());

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					int id = rs.getInt(1);
					Team team = getTeamByID(id);
					teams.add(team);
				}
			}
		} catch (SQLException e) {
			throw new DBException("Can't get user teams", e);
		}

		return teams;
	}

	public Team getTeamByID(int id) {
		Team team = new Team();
		String query = "SELECT name FROM teams WHERE id = ?";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setInt(1, id);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					team.setId(id);
					team.setName(rs.getString(1));
				}
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}

		return team;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE id = ?";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setInt(1, team.getId());

			if (ps.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException e) {
			throw new DBException("Can't delete team", e);
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET name = ? WHERE id = ?";

		try (Connection conn = getConnection();
			 PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, team.getName());
			ps.setInt(2, team.getId());

			if (ps.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException e) {
			throw new DBException("Can't update team", e);
		}

		return true;
	}

	private static void setAutoCommit(boolean value) {
		try {
			connection.setAutoCommit(value);
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
	}

}